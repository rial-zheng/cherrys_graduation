package com.cherrys.schooldemo.service;


import com.cherrys.schooldemo.vo.resp.HomeRespVO;

/**
 * 首页
 *
 */
public interface HomeService {

    HomeRespVO getHomeInfo(String userId);
}
