package com.cherrys.schooldemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cherrys.schooldemo.entity.SysRolePermission;
import com.cherrys.schooldemo.vo.req.RolePermissionOperationReqVO;

/**
 * 角色权限关联
 */
public interface RolePermissionService extends IService<SysRolePermission> {

    void addRolePermission(RolePermissionOperationReqVO vo);
}
