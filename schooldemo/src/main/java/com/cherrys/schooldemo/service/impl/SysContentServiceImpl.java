package com.cherrys.schooldemo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cherrys.schooldemo.entity.SysContentEntity;
import com.cherrys.schooldemo.mapper.SysContentMapper;
import com.cherrys.schooldemo.service.SysContentService;
import org.springframework.stereotype.Service;

/**
 * 内容 服务类
 *
 */
@Service("sysContentService")
public class SysContentServiceImpl extends ServiceImpl<SysContentMapper, SysContentEntity> implements SysContentService {


}