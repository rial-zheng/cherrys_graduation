package com.cherrys.schooldemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cherrys.schooldemo.entity.SysDept;
import com.cherrys.schooldemo.vo.resp.DeptRespNodeVO;

import java.util.List;

/**
 * 部门
 */
public interface DeptService extends IService<SysDept> {

    SysDept addDept(SysDept vo);

    void updateDept(SysDept vo);

    void deleted(String id);

    List<DeptRespNodeVO> deptTreeList(String deptId);

    List<SysDept> selectAll();
}
