package com.cherrys.schooldemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cherrys.schooldemo.entity.SysContentEntity;

/**
 * 内容 服务类
 *
 */
public interface SysContentService extends IService<SysContentEntity> {

}

