package com.cherrys.schooldemo.service.impl;

import com.cherrys.schooldemo.entity.SysDept;
import com.cherrys.schooldemo.entity.SysUser;
import com.cherrys.schooldemo.service.DeptService;
import com.cherrys.schooldemo.service.HomeService;
import com.cherrys.schooldemo.service.PermissionService;
import com.cherrys.schooldemo.service.UserService;
import com.cherrys.schooldemo.vo.resp.HomeRespVO;
import com.cherrys.schooldemo.vo.resp.PermissionRespNode;
import com.cherrys.schooldemo.vo.resp.UserInfoRespVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 首页
 */
@Service
public class HomeServiceImpl implements HomeService {
    @Resource
    private UserService userService;
    @Resource
    private DeptService deptService;
    @Resource
    private PermissionService permissionService;

    @Override
    public HomeRespVO getHomeInfo(String userId) {


        SysUser sysUser = userService.getById(userId);
        UserInfoRespVO vo = new UserInfoRespVO();

        if (sysUser != null) {
            BeanUtils.copyProperties(sysUser, vo);
            SysDept sysDept = deptService.getById(sysUser.getDeptId());
            if (sysDept != null) {
                vo.setDeptId(sysDept.getId());
                vo.setDeptName(sysDept.getName());
            }
        }

        List<PermissionRespNode> menus = permissionService.permissionTreeList(userId);

        HomeRespVO respVO = new HomeRespVO();
        respVO.setMenus(menus);
        respVO.setUserInfo(vo);

        return respVO;
    }
}
