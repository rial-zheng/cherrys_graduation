package com.cherrys.schooldemo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cherrys.schooldemo.entity.SysDictEntity;

/**
 * 数据字典 服务类
 *
 */
public interface SysDictService extends IService<SysDictEntity> {

}

