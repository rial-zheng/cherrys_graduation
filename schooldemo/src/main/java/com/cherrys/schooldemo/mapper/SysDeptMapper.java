package com.cherrys.schooldemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cherrys.schooldemo.entity.SysDept;
import org.apache.ibatis.annotations.Mapper;

/**
 * 部门 Mapper
 *
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {
}