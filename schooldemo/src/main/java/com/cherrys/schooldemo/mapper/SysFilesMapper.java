package com.cherrys.schooldemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cherrys.schooldemo.entity.SysFilesEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件上传 Mapper
 */
@Mapper
public interface SysFilesMapper extends BaseMapper<SysFilesEntity> {
	
}
