package com.cherrys.schooldemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cherrys.schooldemo.entity.SysDictEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典 Mapper
 *
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDictEntity> {
	
}
