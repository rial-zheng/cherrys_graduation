package com.cherrys.schooldemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cherrys.schooldemo.entity.SysContentEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 内容管理 Mapper
 *
 */
@Mapper
public interface SysContentMapper extends BaseMapper<SysContentEntity> {
	
}
