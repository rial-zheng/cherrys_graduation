package com.cherrys.schooldemo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cherrys.schooldemo.entity.SysRolePermission;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色权限 Mapper
 *
 */
@Mapper
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}