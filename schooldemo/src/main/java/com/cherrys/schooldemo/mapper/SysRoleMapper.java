package com.cherrys.schooldemo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cherrys.schooldemo.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色 Mapper
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

}