package com.cherrys.schooldemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cherrys.schooldemo.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户角色 Mapper
 *
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}